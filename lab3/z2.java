import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class z2
{
    public static void main (String[] args) {
	
	
    Scanner in = new Scanner(System.in);
    System.out.print("Podaj ilosc liczb : ");
    int x = in.nextInt();

        int[] a = new int[x];     

        generuj (a); 

        System.out.print("a: ");
        wypisz(a);
		System.out.println("w tablicy jest: "+ ileNieparzystych(a)+" nieparzystych liczb");	
		System.out.println("w tablicy jest: "+ ileParzystych(a)+" parzystych liczb");   
		System.out.println("w tablicy jest: "+ ileDodatnich(a)+ " >0 ");
		System.out.println("w tablicy jest: "+ ileUjemnych(a)+ " <0 ");
		System.out.println("w tablicy jest: "+ ileZerowych(a)+ " ==0 ");
		System.out.println(", pojawia sie: "+ ileMaksymalnych(a)+ " razy");
		System.out.println("suma dodatnich: "+ sumaDodatnich(a));
		System.out.println("suma ujemnych: "+ sumaUjemnych(a));
		System.out.print("odwroc: ");odwrocFragment(a,2,8); wypisz(a);
		System.out.print("signum: "); signum(a);
    }


    public static void generuj (int[] tab) {
        Random r = new Random();
        for (int j = 0; j < tab.length; ++j) {
            tab[j] = r.nextInt(2000)-1000;
        }
    }

    public static void wypisz(int[] tab) {
        for (int el : tab) {
            System.out.print(el + " ");
        }
        System.out.println("");
    }

	public static int ileNieparzystych(int tab[]){
		int ile=0;
			for ( int el : tab){
				if(el%2!=0){
					ile++;
				}
			}
		return ile;
	}
	
	public static int ileParzystych(int tab[]){
		int ile=0;
			for ( int el : tab){
				if(el%2==0){
					ile++;
				}
			}
		return ile;
	}
	
	public static int ileDodatnich(int tab[]){
		int ile=0;
		for (int el : tab){
			if (el>0){
				ile++;
			}
		}
	return ile;
		
	}
	
	public static int ileUjemnych(int tab[]){
		int ile=0;
		for (int el : tab){
			if (el<0){
				ile++;
			}
		}
	return ile;
		
	}
	
	public static int ileZerowych(int tab[]){
		int ile=0;
		for (int el : tab){
			if (el==0){
				ile++;
			}
		}
	return ile;
		
	}


	public static int ileMaksymalnych(int tab[]){
		int max=tab[0];
		int ile=0;
		for (int el : tab){
			if(el>max){
				max=el;
			}
		}	
		for (int el: tab){
			if (el==max){
				ile++;
			}
		}
	System.out.print("maksymalny element tablicy to: "+max);
	
	return ile;	
	}
	
	public static int sumaDodatnich(int tab[]){
			int suma=0;
			for (int el:tab){
				if (el>0){
					suma+=el;
				}
			}
		
	return suma;
	}
	
	public static int sumaUjemnych(int tab[]){
			int suma=0;
			for (int el:tab){
				if (el<0){
					suma+=el;
				}
			}
		
	return suma;
	}
	
	public static void signum(int tab[]){
			for (int i=0; i<tab.length; ++i){
				if (tab[i]<0){
					tab[i]=-1;
				}
				if (tab[i]>0){
					tab[i]=1;
				}
			}
		wypisz(tab);
				
	}
	
	public static void odwrocFragment(int tab[], int l, int p){
			int tmp;
			for(int i=l; l<p; ++l){
				tmp=tab[l];
				tab[l]=tab[p];
				tab[p]=tmp;
				p--;
			}
		
	}


}


