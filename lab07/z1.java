import java.util.*;

public class z1
{
	public static void main(String[] args)
	{
		RachunekBankowy saver1= new RachunekBankowy(2000.0);
		RachunekBankowy saver2= new RachunekBankowy(3000.0);
		
		RachunekBankowy.setRocznaStopaProcentowa(0.4);
		
		saver1.obliczMiesieczneOdsetki();
		System.out.println(saver1.stan());
		saver2.obliczMiesieczneOdsetki();
		System.out.println(saver2.stan());
		
		RachunekBankowy.setRocznaStopaProcentowa(0.5);
		saver1.obliczMiesieczneOdsetki();
		System.out.println(saver1.stan());
		saver2.obliczMiesieczneOdsetki();
		System.out.println(saver2.stan());
		
	}
}


class RachunekBankowy{
	public RachunekBankowy(double saldo){ this.saldo=saldo; }
	public void obliczMiesieczneOdsetki(){ saldo+=(saldo*rocznaStopaProcentowa)/12; }
	public static void setRocznaStopaProcentowa(double stopa){ rocznaStopaProcentowa=stopa; }
	public double stan () {return saldo; }
	
	private static double rocznaStopaProcentowa=0.10;
	private double saldo;
}
